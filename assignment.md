# Web-Gis-Assignment

## Objective
This assignment is used to evaluate understanding geospatial data processing, knowledge of appropriage storage software and formats, data publishing software and visualization that will be relevant for the announced position of Developer/webGIS specialist.

## Guidelines
- Please submit your project as a Gitlab/GitHub repository or a downloadable archive.
- Include clear instructions on how to set up and run your application.
- Include a README.md file with an overview of the applications and any additional information.
- Mention any libraries, frameworks, or APIs used in your project.
- Include instructions for testing the application.

# Data processing
In this repository you will find a CSV file with wind data from Southern hemisphere.
Please write a program that will convert it to a valid GeoJSON. Provide a script/executable that does the transformation and instructions how to use it.

Bonus: Demonstrate which techniques could be used to guarantee the integrity of processed information. How would you ensure the processing pipeline is reproducible in case files have to be reprocessed later?

# Data storage
After processing the raw data it should be ingested into a database. It is your choice in which database to ingest the information.
If you choose to work with PostgreSQL/PostGIS provide a script that loads the data into DB. Assume the DB is running on a localhost. Explain how to run loading script. You can provide a docker compose file if you prefer to work with containerized instances.

Alternatively you can use sqlite, geopackage or similar. *Please explain your choice of data storage backend*. 
Pay attention to selecting right the CRS.

# Data publication
Using the storage  generated in the previous step please publish the data as both WMS and WFS layers. 
Question: When you would publish the data as WFS and when as WMS, and when you would use both?

Suggestion: You can solve this task by installing a geoserver instance on your local machine. In such case please provide screenshots and text summary explaining data publishing steps. Alternatively you can provide a docker compose file that sets a geoserver instance and a script that publishes the data using Geoservers REST API. You can also use a public service like QGIS cloud to publish WMS and WFS layers. Please provide
a summary how you have published the data including text and screenshots.


# Data visualization
Please create a QGIS project that uses published previously WFS layer. Use only the data points that fall into the Dronning Maud Land region. Please add a basemap, e.g. MODIS or Sentinel-2 satellite mosaic as a background, and a WFS layer containing coastline or other information that helps identifying the location. 

Generate a specific style for displaying the data. Export the project file, SLD and QLD styles and add them to the deliverable package/repository. QGIS project here is used as a replacement for a web client.

Using a web mapping application illustrating the layers mentiond above is a bonus. In such case provide a link to the app/fiddle, image tag if application is published as a Docker image, or add source code for the application to the project.
 